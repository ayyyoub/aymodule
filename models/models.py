# -*- coding: utf-8 -*-

from odoo import models, fields, api
import csv
import base64

class ayjournal(models.Model):
    _inherit = 'account.journal'
    tvaId = fields.Integer("TVA ID")

class aypartner(models.Model):
    _inherit = 'res.partner'

    id_fiscale = fields.Char("identification fiscale")
    ice = fields.Char("ice")

class aycompany(models.Model):
    _inherit = 'res.company'

    identifiantFiscal = fields.Char("identification fiscale")
    regime_id = fields.Many2one(comodel_name="res.regime", string="Régime",select=True)

class regime(models.Model):
    _name = 'res.regime'

    name = fields.Char("name")
    num = fields.Integer("number")

class aypartner(models.Model):
    _inherit = 'res.partner'

    id_fiscale = fields.Char("identification fiscale")
    ice = fields.Char("ice")

class ayinvoice(models.Model):
    _inherit = 'account.invoice'

    prorata = fields.Float("Prorata")


    @api.one
    @api.depends('invoice_line_ids')
    def _des(self):
        curdes = ""
        for invoice_line_id in self.invoice_line_ids:
            curdes = curdes + '{}'.format(invoice_line_id.product_id.name) + ", "
        print(curdes)
        self.des = curdes

    @api.one
    @api.depends('partner_id')
    def _partnerName(self):
        print(self.partner_name)
        self.partner_name = self.partner_id.name

    @api.one
    @api.depends('partner_id')
    def _partnerIdFiscale(self):
        print(self.partner_id.id_fiscale)
        self.partner_id_fiscale = self.partner_id.id_fiscale

    @api.one
    @api.depends('partner_id')
    def _partnerIce(self):
        print(self.partner_id.ice)
        self.partner_ice = self.partner_id.ice

    @api.one
    @api.depends('amount_untaxed','amount_tax')
    def _txCalculator(self):
        if self.amount_untaxed == 0 :
            self.tx = 0
        else :
            self.tx = self.amount_tax/self.amount_untaxed*100
        print("%.2f -------------" % self.tx)

    @api.one
    @api.depends('payment_ids')
    def _mp_ids(self):
        seqMpIds = ""
        for payment_id in self.payment_ids:
            seqMpIds = str(payment_id.journal_id.tvaId)

        self.mp_ids = seqMpIds
        print(seqMpIds)

    @api.one
    @api.depends('payment_ids')
    def _dpai(self):
        if(list(self.payment_ids)):
            self.dpai = list(self.payment_ids)[0].payment_date

    des = fields.Text(string="des",store=True,compute='_des')
    partner_name = fields.Char(string="partner_name",store=True,compute='_partnerName')
    partner_id_fiscale = fields.Char(string="partner_id_fiscale",store=True,compute='_partnerIdFiscale')
    partner_ice = fields.Char(string="partner_ice",store=True,compute='_partnerIce')
    tx = fields.Integer(string="tx",store=True,compute='_txCalculator')
    mp_ids = fields.Char(string="mp_ids",store=True,compute='_mp_ids')
    dpai = fields.Date(string="dpai",store=True,compute='_dpai')


class aymodule(models.Model):
    _name = 'aymodule.aymodule'

    title = fields.Char(string="Nom de fichier",default="DeclarationReleveDeduction.xml")
    csv_file = fields.Binary(string='CSV File', required=True)
    xml_file = fields.Binary('XML File')

    company_id = fields.Many2one(comodel_name="res.company", string="id",select=True)
    identifiantFiscal = fields.Char(string="identifiantFiscal", required=True, default='1003928')
    Annee = fields.Char(string="Année", required=True, default='2018')
    periode = fields.Char(string="periode", required=True, default='10')
    regime_id = fields.Many2one(comodel_name="res.regime", string="Régime",select=True)

    @api.onchange('company_id')
    def _onchange_project_id(self):
        self.identifiantFiscal = self.company_id.identifiantFiscal
        self.regime_id = self.company_id.regime_id

    @api.depends('csv_file','identifiantFiscal','Année','periode','Régime')
    def import_csv(self):
        reader = csv.DictReader(base64.b64decode(self.csv_file).decode('utf-8').split('\n'))
        output_xml = '<?xml version="1.0" encoding="UTF-8"?>\n'
        output_xml += '<DeclarationReleveDeduction>\n'

        output_xml += '{:s}<{:s}>'.format(self.indent(1), 'identifiantFiscal')
        output_xml += self.convert_metacharacters('{:s}'.format(self.identifiantFiscal))
        output_xml += '</{:s}>\n'.format('identifiantFiscal')

        output_xml += '{:s}<{:s}>'.format(self.indent(1), 'annee')
        output_xml += self.convert_metacharacters('{:s}'.format(self.Annee))
        output_xml += '</{:s}>\n'.format('annee')

        output_xml += '{:s}<{:s}>'.format(self.indent(1), 'periode')
        output_xml += self.convert_metacharacters('{:s}'.format(self.periode))
        output_xml += '</{:s}>\n'.format('periode')

        output_xml += '{:s}<{:s}>'.format(self.indent(1), 'regime')
        output_xml += self.convert_metacharacters('{}'.format(self.regime_id.num))
        output_xml += '</{:s}>\n'.format('regime')



        output_xml += '{:s}{:s}\n'.format(self.indent(1), '<releveDeductions>')
        i = 1
        for row in reader:
            output_xml += '{:s}{:s}\n'.format(self.indent(2), '<rd>')

            output_xml += '{:s}<{:s}>'.format(self.indent(3), 'ord')
            output_xml += self.convert_metacharacters('{:s}'.format(str(i)))
            output_xml += '</{:s}>\n'.format('ord')

            output_xml += '{:s}<{:s}>'.format(self.indent(3), 'num')
            output_xml += self.convert_metacharacters('{:s}'.format(row['number']))
            output_xml += '</{:s}>\n'.format('num')

            output_xml += '{:s}<{:s}>'.format(self.indent(3), 'des')
            output_xml += self.convert_metacharacters('{:s}'.format(row['des']))
            output_xml += '</{:s}>\n'.format('des')

            output_xml += '{:s}<{:s}>'.format(self.indent(3), 'mht')
            output_xml += self.convert_metacharacters('{:s}'.format(row['amount_untaxed']))
            output_xml += '</{:s}>\n'.format('mht')

            output_xml += '{:s}<{:s}>'.format(self.indent(3), 'tva')
            output_xml += self.convert_metacharacters('{:s}'.format(row['amount_tax']))
            output_xml += '</{:s}>\n'.format('tva')

            output_xml += '{:s}<{:s}>'.format(self.indent(3), 'ttc')
            output_xml += self.convert_metacharacters('{:s}'.format(row['amount_total']))
            output_xml += '</{:s}>\n'.format('ttc')

            # fournissuer
            output_xml += '{:s}{:s}\n'.format(self.indent(3), '<refF>')
            output_xml += '{:s}<{:s}>'.format(self.indent(4), 'if')
            output_xml += self.convert_metacharacters('{:s}'.format(row['partner_id_fiscale']))
            output_xml += '</{:s}>\n'.format('if')

            output_xml += '{:s}<{:s}>'.format(self.indent(4), 'nom')
            output_xml += self.convert_metacharacters('{:s}'.format(row['partner_name']))
            output_xml += '</{:s}>\n'.format('nom')

            output_xml += '{:s}<{:s}>'.format(self.indent(4), 'ice')
            output_xml += self.convert_metacharacters('{:s}'.format(row['partner_ice']))
            output_xml += '</{:s}>\n'.format('ice')
            output_xml += '{:s}{:s}\n'.format(self.indent(3), '</refF>')


            output_xml += '{:s}<{:s}>'.format(self.indent(3), 'tx')
            output_xml += self.convert_metacharacters('{:s}'.format(row['tx']))
            output_xml += '</{:s}>\n'.format('tx')

            output_xml += '{:s}<{:s}>'.format(self.indent(3), 'prorata')
            output_xml += self.convert_metacharacters('{:s}'.format(row['prorata']))
            output_xml += '</{:s}>\n'.format('prorata')

            output_xml += '{:s}<{:s}>\n'.format(self.indent(3), 'mp')
            output_xml += '{:s}<{:s}>'.format(self.indent(4), 'id')
            output_xml += self.convert_metacharacters('{:s}'.format(row['mp_ids']))
            output_xml += '</{:s}>\n'.format('id')
            output_xml += '{:s}</{:s}>\n'.format(self.indent(3), 'mp')

            output_xml += '{:s}<{:s}>'.format(self.indent(3), 'dpai')
            output_xml += self.convert_metacharacters('{:s}'.format(row['dpai']))
            output_xml += '</{:s}>\n'.format('dpai')

            output_xml += '{:s}<{:s}>'.format(self.indent(3), 'dfac')
            output_xml += self.convert_metacharacters('{:s}'.format(row['date_invoice']))
            output_xml += '</{:s}>\n'.format('dfac')



            output_xml += '{:s}{:s}\n'.format(self.indent(2), '</rd>')

            i = i + 1

        output_xml += '{:s}{:s}\n'.format(self.indent(1), '</releveDeductions>')
        output_xml += '</DeclarationReleveDeduction>'


        self.xml_file = base64.b64encode(output_xml.encode('utf-8'))
        print(output_xml)
        partners = self.env['res.partner'].search([])
        for item in partners:
            print(item.city)

        # import os.path
        #
        # my_path = os.path.abspath(os.path.dirname(__file__))
        # path = os.path.join(my_path, "../data/invoices.xml")
        #
        # file = open(path, 'wb+')
        # if file:
        #     file.write(base64.b64decode(self.xml_file))
        #     wizard = self.env['exp_xml'].with_context({'active_id': self.id})
        # I have found this on the web
        # return {
        #     'type': 'ir.actions.act_url',
        #     'url': '/web/binary/saveas?model=ir.attachment&field=datas&id=%s&filename_field=self.xml_file' % (
        #         wizard.id),
        #     'target': 'self',
        # }
        # return {
        #     'type': 'ir.actions.act_url',
        #     'url': '/web/binary/download_document?model=exp_xml&field=xml_file&id=%s&filename=demoxml.xml' % (
        #         self.id),
        #     'target': 'self',
        # }

    def convert_metacharacters(self, s):
        s = s.replace("&", "&amp;")
        s = s.replace("<", "&lt;")
        s = s.replace(">", "&gt;")
        #         s = s.replace("\'", "&apos;")
        s = s.replace('\"', "&quot;")
        s = s.replace('\r', '')
        return s

    def indent(self, n):
        # str = '\t' * n
        s = '    ' * n
        return s
